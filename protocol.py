# Gemini protocol interface

from os import environ
import sys
from urllib.parse import unquote

from .validate import valid_url


class Request:
    """Represents a Gemini request"""
    
    def __init__(self):
        self.url: str      = None
        self.protocol: str = None
        self.host: str     = None
        self.port: str     = None
        self.path: str     = None
        self.query: str    = None
        self.params        = {}  # path parameters

    def from_url(url: str) -> 'Request':
        """Parses a URL into a Request, also performing basic URL validation"""
        # FIXME: spec non-compliance: rejects empty path component instead of
        #        treating it as equivalent to '/'
        if not valid_url(url):
            raise ValueError('not a valid URL: ' + url)
        
        request = Request()
        request.url      = url
        request.protocol = url.split('://', 1)[0]
        host_port_pair   = url.split('://', 1)[1].split('/', 1)[0]
        request.host     = host_port_pair.split(':', 1)[0]
        request.port     = int(host_port_pair.split(':', 1)[1]) \
                           if ':' in host_port_pair else 1965
        path_query_pair  = '/' + url.split('://', 1)[1].split('/', 1)[1]
        request.path     = unquote(path_query_pair.split('?', 1)[0])
        request.query    = unquote(path_query_pair.split('?', 1)[1]) \
                           if '?' in path_query_pair else ''

        return request

    def from_cgi() -> 'Request':
        """Creates a Request based on CGI environment variables"""
        request = Request()
        request.url      = environ['GEMINI_URL']
        request.protocol = environ['SERVER_PROTOCOL'].lower()
        request.host     = environ['SERVER_NAME']
        request.port     = int(environ['SERVER_PORT'])
        request.path     = environ['PATH_INFO']
        request.query    = unquote(environ['QUERY_STRING'])
        
        return request

    def from_str(data: str) -> 'Request':
        """Parses a string (as received from client) into a Request, also
        performing basic URL validation"""
        return Request.from_url(data.rstrip())


class Response:
    """Represents a Gemini response"""

    class Code:
        INPUT                       = 10
        SENSITIVE_INPUT             = 11
        SUCCESS                     = 20
        REDIRECT_TEMPORARY          = 30
        REDIRECT_PERMANENT          = 31
        TEMPORARY_FAILURE           = 40
        SERVER_UNAVAILABLE          = 41
        CGI_ERROR                   = 42
        PROXY_ERROR                 = 43
        SLOW_DOWN                   = 44
        PERMANENT_FAILURE           = 50
        NOT_FOUND                   = 51
        GONE                        = 52
        PROXY_REQUEST_REFUSED       = 53
        BAD_REQUEST                 = 59
        CLIENT_CERTIFICATE_REQUIRED = 60
        CERTIFICATE_NOT_AUTHORISED  = 61
        CERTIFICATE_NOT_VALID       = 62
    
    def __init__(self):
        self.code: int      = None
        self.meta: str      = None
        self.body: bytes    = None
        self.sent: bool     = False
        self.streamed: bool = False

    def __str__(self) -> str:
        header = f'{self.code} {self.meta or ""}'
        body = self.body or ''
        return header + '\r\n' + body

    def send(self):
        """Sends the response to the client"""
        # check this is actually a sensible thing to do right now
        if self.sent:
            raise ValueError('Response already sent')
        if self.code is None:
            raise ValueError('Response code not set')
        if self.streamed:
            return

        # send it by printing to stdout to be received by the tcp handler
        print(str(self), end='')

        self.sent = True

    def stream(self):
        """Returns a stream that sends its data to the client"""
        # check if this is sensible
        if self.sent or self.streamed:
            raise ValueError('Response already sent')
        if self.code is None:
            raise ValueError('Response code not set')

        self.body = None
        print(str(self), end='')
        sys.stdout.flush()
        self.streamed = True
        
        return sys.stdout
