# Slightly more structured server framework using a super basic router

import re
import sys

from .protocol import *

handlers = {}

def route(path: str) -> callable:
    """Annotation to register a route handler"""
    def inner(fn: callable) -> callable:
        handlers[path] = fn
        return fn
    return inner


def match_handler(path: str) -> tuple[callable, dict[str, str]]:
    """Get the handler for given path as well as path parameters"""
    for pattern in handlers.keys():
        regex_pattern = '^' + re.sub('\\{[^}]+\\}', '([^/]+)', pattern) + '$'
        if re.match(regex_pattern, path):
            handler = handlers[pattern]
            params_keys = re.findall('\\{([^}]+)\\}', pattern)
            params_vals = re.findall(regex_pattern, path)[0]
            # this is required due to very inconsistent return value of findall
            if type(params_vals) == tuple:
                params_vals = list(params_vals)
            else:
                params_vals = [params_vals]
            params = dict(zip(params_keys, params_vals))
            return handler, params
    return None, None


def process_request(hosts: list[str] = ['localhost', '127.0.0.1'],
                    cgi_mode: bool = True):
    """Process the request on stdin. `hosts` is checked against the requested
    hostname to deny proxy requests. `cgi_mode=False` causes it to get request
    data from a line of input instead of environment variables"""
    resp = Response()
    # default response in case one isn't set later
    resp.code = Response.Code.CGI_ERROR
    try:
        req: Request = None
        if cgi_mode:
            req = Request.from_cgi()
        else:
            req = Request.from_str(input())

        # don't allow proxying
        if req.protocol != 'gemini' or req.host not in hosts:
            resp.code = Response.Code.PROXY_REQUEST_REFUSED
            return
        
        # if a handler is registered for this path, call it
        handler, params = match_handler(req.path)
        if handler is not None:
            try:
                req.params = params
                handler(req, resp)
            except Exception as e:  # something mysterious went wrong
                resp.code = Response.Code.CGI_ERROR
                resp.meta = 'handler: ' + str(e)
                
        # otherwise return a "not found"
        else:
            resp.code = Response.Code.NOT_FOUND
            
    except ValueError as e:  # invalid URL most likely
        resp.code = Response.Code.BAD_REQUEST
        resp.meta = str(e)
    
    except Exception as e:  # other error outside of the handler
        resp.code = Response.Code.CGI_ERROR
        resp.meta = 'process_request: ' + str(e)

    finally:
        resp.send()
