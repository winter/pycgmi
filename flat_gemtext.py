# Gemtext output utilities

def heading(depth: int, text: str) -> str:
    """A heading of the given depth (1 being title)"""
    return f'{"#" * depth} {text}\n'

# shorthand for common heading depths
title      = lambda text: heading(1, text)
section    = lambda text: '\n' + heading(2, text)
subsection = lambda text: '\n' + heading(3, text)

def link(label: str, target: str) -> str:
    """A link line with given label and target URL"""
    # TODO: normalise target URL
    return f'=> {target} {label}\n'

def bullet(text: str) -> str:
    """A bullet point"""
    return f'* {text}\n'

def preformatted(label: str, body: str) -> str:
    """A preformatted block. You should specify label for accessibility reasons
    but it's not required"""
    return f'```{label}\n{body}\n```\n'

def blockquote(text: str) -> str:
    """A block quote"""
    return f'> {text}\n'

def paragraph(text: str) -> str:
    """A simple paragraph"""
    return f'{text}\n'
