# too-lenient validator for URLs
# works well for this purpose and probably not for much else :)
# previously used validators library, but it enforces the use of the schemes
# http(s) or ftp, so it's no good whatsoever

import re

VALID_URL_REGEX = re.compile('^[-+a-z0-9]+:\/\/[-a-z0-9:[\]\.]+\/')

def valid_url(url: str) -> bool:
    """Checks if the given URL is valid"""
    return re.match(VALID_URL_REGEX, url) is not None
