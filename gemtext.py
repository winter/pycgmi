# Structured gemtext utilities

# type for gemtext "components" (actually just strings)
Component = str


def article(title: str, *children: list[Component]) -> Component:
    """An article, i.e. a self-contained unit of material with a title"""
    return f'# {title}\n' + ''.join(children) + '\n'


def section(heading: str, *children: list[Component]) -> Component:
    """A section, i.e. a group of related material within an article"""
    return f'\n## {heading}\n' + ''.join(children)


def subsection(heading: str, *children: list[Component]) -> Component:
    """A section within a section"""
    return f'\n### {heading}\n' + ''.join(children)


def paragraph(body: str) -> Component:
    """A paragraph of prose or line of poetry"""
    return body + '\n'


def hyperlink(target: str, label: str = '') -> Component:
    """A link to another document"""
    if len(label) > 0: label = ' ' + label
    return f'=> {target}{label}\n'


def bullet_list(*items: list[str]) -> Component:
    """An unordered list of (usually short) items"""
    return ''.join([f'* {e}\n' for e in items])


def blockquote(body: str) -> Component:
    """A quoted paragraph or paragraphs"""
    return ''.join([f'> {line}\n' for line in body.splitlines()])


def preformatted(body: str, label: str = '') -> Component:
    """A block of preformatted text (e.g. code or ASCII art), usually displayed
    in a fixed-width font. `label` should contain a summary or description of
    the `body`, for accessibility purposes."""
    return f'```{label}\n{body}\n```'
